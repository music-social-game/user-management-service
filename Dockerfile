FROM python:3.9-slim

ENV PYTHONUNBUFFERED 1

RUN apt update && apt install git libxml2 libxml2-dev libxslt-dev gcc python3-dev musl-dev -y

RUN mkdir -p /code

WORKDIR /code

COPY Pipfile Pipfile.lock /code/

RUN pip install pipenv
RUN pipenv install --system

COPY ./run.sh /code/
COPY ./alembic.ini /code/
COPY ./app /code/app/
COPY ./alembic /code/alembic/
WORKDIR /code/

EXPOSE 8080
