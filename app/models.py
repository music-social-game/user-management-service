from sqlalchemy.orm import declarative_base, relationship
from sqlalchemy import Column, Integer, String, Boolean, ForeignKey
from sqlalchemy.dialects.postgresql import UUID, BYTEA, JSON

Base = declarative_base()


class User(Base):
    __tablename__ = 'user'

    id = Column(Integer, primary_key=True)
    username = Column(String, nullable=False, index=True)
    password = Column(String)
    email = Column(String, nullable=False, index=True)
    is_active = Column(Boolean, nullable=False, default=False)


class Confirmation(Base):
    __tablename__ = 'confirmation'

    confirmation_id = Column(Integer, primary_key=True)
    user_id = Column(Integer, ForeignKey("user.id", ondelete="CASCADE", name='user_conf_constr'), nullable=False)
    uuid = Column(UUID, nullable=False, index=True)
    user = relationship("User", backref='confirmation')


class Token(Base):
    __tablename__ = 'token'

    id = Column(Integer, primary_key=True)
    user_id = Column(Integer, ForeignKey("user.id", ondelete="CASCADE", name='user_token_constr'), nullable=False)
    spotify_token = Column(BYTEA, nullable=False)
    user = relationship("User", backref='token')


class MusicData(Base):
    __tablename__ = 'usermusicdata'

    id = Column(Integer, primary_key=True)
    user_id = Column(Integer, ForeignKey("user.id", ondelete="CASCADE", name='user_musicdata_constr'), nullable=False)
    spotify_data = Column(JSON, nullable=False)
    user = relationship("User", backref='usermusicdata')