from pydantic import BaseSettings
from pathlib import Path

ENV_PATH = Path.cwd() / '.env'


class Settings(BaseSettings):
    host: str
    postgres_db: str = 'db'
    postgres_user: str = 'user'
    postgres_password: str = 'password'
    postgres_host: str = 'localhost'
    postgres_port:  str = 5432
    smtp_host:  str
    smtp_port:  int
    smtp_user:  str
    smtp_password:  str
    mq_host:  str
    mq_port:  int
    mq_user:  str
    mq_pass:  str
    user_routing_key:  str
    authjwt_secret_key: str = "secret"
    authjwt_access_token_expires: int
    authjwt_refresh_token_expires: int


    class Config:
        env_file = ENV_PATH


settings = Settings()


DATABASE_URL = f"postgresql://{settings.postgres_user}:{settings.postgres_password}@" \
               f"{settings.postgres_host}:{settings.postgres_port}/{settings.postgres_db}"


MQ_URL = f"amqp://{settings.mq_user}:{settings.mq_pass}@{settings.mq_host}:{settings.mq_port}/"