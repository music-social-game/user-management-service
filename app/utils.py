import base64
from .models import Token as DbToken
from sqlalchemy import insert, update

def make_password_hash(pwd_context, password):
    return pwd_context.hash(password)


async def process_spotify(token_id, user_id, token, database):
    if not token_id:
        query = insert(DbToken).values(user_id=user_id,
                                       spotify_token=base64.b64decode(token.token.encode()))
        token_id = await database.execute(query)
    else:
        query = update(DbToken).where(DbToken.id == token_id).values(
                                     spotify_token=base64.b64decode(token.token.encode()))
        await database.execute(query)
    return token_id