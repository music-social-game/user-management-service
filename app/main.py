import json
import aio_pika
import uvicorn
import databases
import asyncio
from fastapi import FastAPI, status, Depends
from .models import User as DbUser, Confirmation as DbConfirmation, Token as DbToken
from .schema import User as SchemaUser, Confirmation as SchemaConfirmation, \
    LoginUser, UserShort, Token, UserToken
from sqlalchemy import select, insert, or_, update, delete
from fastapi.exceptions import HTTPException
from passlib.context import CryptContext
from pydantic import UUID4
from aio_pika import ExchangeType, Message, DeliveryMode
from fastapi_jwt_auth import AuthJWT
from .config import DATABASE_URL, MQ_URL, settings, Settings
from .utils import make_password_hash, process_spotify

app = FastAPI()
database = databases.Database(DATABASE_URL)
pwd_context = CryptContext(schemes=["bcrypt"], deprecated="auto")
AuthJWT.load_config(Settings)


@app.on_event("startup")
async def startup():
    attempt = 10
    while True:
        try:
            attempt -= 1
            mq_connection = await aio_pika.connect_robust(MQ_URL)
            break
        except ConnectionError:
            if attempt > 0:
                print("Reestablish connection to MQ")
                await asyncio.sleep(10)
            else:
                raise
    channel = await mq_connection.channel()
    user_exchange = await channel.declare_exchange("users", ExchangeType.DIRECT)
    setattr(app, 'user_exchange', user_exchange)
    await database.connect()


@app.on_event("shutdown")
async def shutdown():
    await database.disconnect()


@app.get("/")
async def root():
    return {"message": "hello world user manager"}


@app.post('/make_user', status_code=status.HTTP_200_OK)
async def make_user(user_in: SchemaUser):
    query = select(DbUser.username).where(or_(DbUser.username == user_in.username, DbUser.email == user_in.email))
    user = await database.execute(query)
    if user:
        raise HTTPException(status.HTTP_400_BAD_REQUEST, "User with this username or email already exist")
    password = make_password_hash(pwd_context, user_in.password)
    query = insert(DbUser).values(username=user_in.username,
                                  password=password,
                                  email=user_in.email,
                                  is_active=user_in.confirmed)
    user_id = await database.execute(query)
    user = user_in.dict()
    user.update({'id': user_id})
    payload = {'InnerUser': user, 'action': 'send_confirm_message'}
    message_body = bytes(json.dumps(payload), 'utf-8')
    message = Message(
        message_body,
        delivery_mode=DeliveryMode.PERSISTENT
    )
    await app.user_exchange.publish(message, routing_key=settings.user_routing_key)


@app.put('/confirm_email', status_code=status.HTTP_200_OK)
async def confirm_email(uuid: UUID4):
    query = select(DbConfirmation).where(DbConfirmation.uuid == uuid)
    value = await database.fetch_one(query)
    if value:
        conf = SchemaConfirmation(**value)
        update_query = update(DbUser).where(DbUser.id == conf.user_id).values(is_active=True)
        delete_query = delete(DbConfirmation).where(DbConfirmation.uuid == uuid)
        async with database.connection() as connection:
            async with connection.transaction():
                await database.execute(update_query)
                await database.execute(delete_query)
    else:
        raise HTTPException(status.HTTP_400_BAD_REQUEST)


@app.post('/login_email', status_code=status.HTTP_200_OK)
async def login_email(user: LoginUser, Authorize: AuthJWT = Depends()):
    if user.username:
        query = select(DbUser).where(DbUser.username == user.username)
    elif user.email:
        query = select(DbUser).where(DbUser.email == user.email)
    value = await database.fetch_one(query)
    if not value or not pwd_context.verify(user.password, value['password']):
        raise HTTPException(status_code=401, detail="Bad username or password")
    elif not value['is_active']:
        raise HTTPException(status_code=400, detail="User not activated")
    access_token = Authorize.create_access_token(subject=user.email)
    refresh_token = Authorize.create_refresh_token(subject=user.email)
    return {"access_token": access_token, "refresh_token": refresh_token}


@app.post('/refresh_token')
def refresh_token(Authorize: AuthJWT = Depends()):
    Authorize.jwt_refresh_token_required()
    current_user = Authorize.get_jwt_subject()
    new_access_token = Authorize.create_access_token(subject=current_user)
    return {"access_token": new_access_token}


@app.get('/get_user', status_code=status.HTTP_200_OK)
async def get_user(Authorize: AuthJWT = Depends()):
    Authorize.jwt_required()
    user = Authorize.get_jwt_subject()
    query = select(DbUser.id, DbUser.username, DbUser.email).where(DbUser.email == user)
    value = await database.fetch_one(query)
    if value:
        return UserShort(**value)
    raise HTTPException(status.HTTP_400_BAD_REQUEST, {"user": "wrong user data"})


@app.post('/social_auth', status_code=status.HTTP_200_OK)
async def social_auth(user_in: SchemaUser, Authorize: AuthJWT = Depends()):
    query = select(DbUser.username).where(DbUser.email == user_in.email)
    user = await database.execute(query)
    if not user:
        query = insert(DbUser).values(username=user_in.username,
                              password=None,
                              email=user_in.email,
                              is_active=user_in.confirmed)
        await database.execute(query)
    access_token = Authorize.create_access_token(subject=user_in.email)
    refresh_token = Authorize.create_refresh_token(subject=user_in.email)
    return {"access_token": access_token, "refresh_token": refresh_token}

service_map = {
    'spotify': process_spotify
}

@app.post('/set_token', status_code=status.HTTP_200_OK)
async def set_token(token: Token, Authorize: AuthJWT = Depends()):
    Authorize.jwt_required()
    user_email = Authorize.get_jwt_subject()
    query = select(DbUser.id).where(DbUser.email == user_email)
    user_id = await database.execute(query)
    query = select(DbToken.id).where(DbToken.user_id == user_id)
    token_id = await database.execute(query)
    method = service_map[token.service]
    token_id = await method(token_id, user_id, token, database)
    usertoken = UserToken(token_id=token_id, service=token.service)
    payload = {'UserToken': usertoken.dict(), 'action': 'update_user_music_data'}
    message_body = bytes(json.dumps(payload), 'utf-8')
    message = Message(
        message_body,
        delivery_mode=DeliveryMode.PERSISTENT
    )
    await app.user_exchange.publish(message, routing_key=settings.user_routing_key)

if __name__ == "__main__":
    uvicorn.run(app, host="0.0.0.0", port=8000)
