from pydantic import BaseModel, EmailStr, UUID4
from typing import Optional


class LoginUser(BaseModel):
    username: Optional[str]
    password: str
    email: Optional[EmailStr]


class User(BaseModel):
    username: str
    password: Optional[str]
    email: EmailStr
    confirmed: bool


class UserShort(BaseModel):
    id: str
    username: str
    email: Optional[EmailStr]

    class Config:
        orm = True


class Confirmation(BaseModel):
    confirmation_id: int
    user_id: int
    uuid: UUID4


class Token(BaseModel):
    token: str
    service: str


class UserToken(BaseModel):
    token_id: int
    service: str